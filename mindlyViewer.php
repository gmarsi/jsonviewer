<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Document</title>
		<link rel="stylesheet" href="style/style.css">
		<script
		src="https://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>
	</head>
	<body>
		
		<p id="title"></p>
		<div id="content"></div>
		<?php
			require('./server/upload_file.php');
		?> 
		<input type="text" hidden id="myFileId" value=<?php echo $id; ?>>
		<script src="script/script.js"></script>
	</body>
</html>