var myFileId = 'Titulo' //document.getElementById('myFileId').value;
var childsOfThisLevel = 0
var json;
var titles = []
// titulo principal
var title = ""
$.ajax({    
    cache: "false",
    type: "GET",
    url: 'uploads/' + myFileId + '.opml',
    dataType: "xml",
    success: function(xml) {
    	// console.log("Success on load file")
        json = xmlToJson(xml)
        console.log(json.opml);
        readOpml(json)
        positionOuterSpheres()
    }
});

// Parses XML to JSON
function xmlToJson(xml) {	
	// Create the return object
	var obj = {};
	if (xml.nodeType == 1) { // element
		// do attributes
		if (xml.attributes.length > 0) {
		obj["attributes"] = {};
			for (var j = 0; j < xml.attributes.length; j++) {
				var attribute = xml.attributes.item(j);
				obj["attributes"][attribute.nodeName] = attribute.nodeValue;
			}
		}
	} else if (xml.nodeType == 3) { // text
		obj = xml.nodeValue;
	}
	// do children
	if (xml.hasChildNodes()) {
		for(var i = 0; i < xml.childNodes.length; i++) {
			var item = xml.childNodes.item(i);
			var nodeName = item.nodeName;
			nodeName == '#text' ? nodeName = 'text' : null
			if (typeof(obj[nodeName]) == "undefined") {
				obj[nodeName] = xmlToJson(item);
			} else {
				if (typeof(obj[nodeName].push) == "undefined") {
					var old = obj[nodeName];
					obj[nodeName] = [];
					obj[nodeName].push(old);
				}
				obj[nodeName].push(xmlToJson(item));
			}
		}
	}
	return obj;
};

var recu = 0
var arrayData = []
var arrayAux = arrayData
var openElements = []
var currentLevelData = []

function readOpml(json){
	var opml = json.opml
	title = opml.head.title.text
	titles.push(title)
	$("#mindlyContent").append('<button id="0" class="centerCircle"><div id="innerCenterCircleDiv0" class="innerCenterCircleDiv"><span id="title0">' + title + '</span></div></button>')
	var body = opml.body
	// If has childs
	if(body.hasOwnProperty('outline')){
		// iIf has more than 1 child
		if(body.outline.hasOwnProperty('outline') || body.outline.length > 1){
			var bodyLength = body.outline.length		
			var bodyElements = body.outline
			childsOfThisLevel = bodyLength
			for(var i = 0; i < bodyLength; i++){
				$("#mindlyContent").append('<button id="0'+i+'" class="circle hide element fadeOut"><div id="innerCenterCircleDiv0'+i+'" class="innerCenterCircleDiv"><span id="title0'+i+'">' + bodyElements[i].attributes.text + '</span></div></button>')
				$("#mindlyContent").append('<button id="sphereAdd0' + i + '" class="sphereAdd hide fadeOut" style="left: 792px; top: 301px;">+</button>')
				arrayData.push(bodyElements[i])
			}
		// If only has 1 child
		}else{
			$("#mindlyContent").append('<button id="0'+i+'" class="circle element fadeOut"><div id="innerCenterCircleDiv0'+i+'" class="innerCenterCircleDiv"><span id="title0'+i+'">' + body.outline.attributes.text + '</span></div></button>')
			$("#mindlyContent").append('<button id="sphereAdd0' + i + '" class="sphereAdd fadeOut" style="left: 792px; top: 301px;">+</button>')
		}

	}
	// getLastCornerTitle('0')

}




// Nos va dando los valores del id
function* generator(split){
	// Obtenemos split en forma de array
	// Iteramos sobre cada una de las posiciones devolviendo el valor de ellas
	for(var i = 0; i < split.length; i++){
		yield split[i]
	}
}


function setNewLevel(id){
	try {
		getLastCornerTitle(id.substring(0, id.length - 2))
	} catch (error) {
		// console.log('We are at home');
	}
	id = id.substring(1, id.length)
	// console.log(id);

	// if(!continueScript){
		console.log('no continuamos');
	// 	return false;
	// }
	arrayAux = arrayData
	// Hacemos un split del id para obtener los diferentes niveles
	var split = id.split("")
	// Pasamos al generador el array con los diferentes niveles
	var gen = generator(split)
	// Obtenemos el valor del generador de la primera posicion de split
	var lvlAux = gen.next()
	// Obtenemos el primer valor de la primera posicion del split
	var lvl = lvlAux.value
	// Para el bucle el limite sera el valor del generador + 1
	var limit =  parseInt(lvl) + 1
	// Hacemos un bucle hasta el valor del numero generado
	for(var i = 0; i < limit; i++){
		if(i == lvl){
			// Entramos aqui cuando el valor (lvl) del generador corresponda con i
			// Obtenemos el valor del array en la posicion que queremos
			if(arrayAux.hasOwnProperty('outline')){
				if(arrayAux.outline.hasOwnProperty('outline')){
					arrayAux = arrayAux.outline
				}else{
					arrayAux = arrayAux.outline[lvl]
				}				
			}else{
				arrayAux = arrayAux[lvl]
			}
			// hacemos un reset del bucle.
			// si lo ponemos a 0, al volver a empezar se incrementa a 1 y se termina el bucle
			i = -1
			// Obtenemos los valores del siguiente generador
			lvlAux = gen.next()
			// Comprobamos si el siguiente valor existe o el generador ya ha terminado
			if(!isNaN(lvlAux.value)){
				// lvl sera la posicion en el array que buscamos
				lvl = lvlAux.value
				// limit es el maximo de vueltas del bucle. Incrementado en 1
				limit = parseInt(lvl) + 1
				if(lvlAux.done){
					// console.log('Informacion a mostrar');
					// console.log(arrayAux);
					// El generador ha terminado
					break
				}else{
				}
			}else{
				// console.log('Informacion a mostrar');
				// console.log(arrayAux);
				// El generador ha terminado
				break
			}
		}
		

	}
	
	try{
		if(arrayAux.outline.hasOwnProperty('outline')){
			childsOfThisLevel = 1
		}else{
			childsOfThisLevel = arrayAux.outline.length
		}
		return true
	}catch(e){
		alert('There is no more levels!')
		return false
	}

	// getInfo(id)
}

function manageTitles(action){
	if(action === 'add'){
		titles.push(arrayAux.attributes.text)
	}else if(action === 'substract'){
		titles.pop()
	}
	console.log(titles)
	
}

// Mostramos la informacion del hijo
function getInfo(parentId){
	// var color = getRandomColor()
	console.log(arrayAux);
	
	if(arrayAux != undefined){
		if(arrayAux.hasOwnProperty('outline')){
			if(arrayAux.outline.hasOwnProperty('outline')){
				// console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaa');
				// console.log("#title" + parentId + "0")
				$("#title" + parentId + "0").text(arrayAux.outline.attributes.text)
				console.log(arrayAux.outline.attributes.text);
				
			}else{
				// console.log('bbbbbbbbbbbbbbbbbbbbb');
				for(var i = 0; i < childsOfThisLevel; i++){
					newId = parentId + i
					$("#title" + newId).text(arrayAux.outline[i].attributes.text)
				}
			}
		}else{
			// Aqui no deberia entrar nunca, pero por si las moscas
			alert('No hay mas datos en este nivel')
		}
	}else{
		alert('No hay mas datos para mostrar')
	}
}

// Comprobamos si la informacion se esta mostrando o esta oculta
function checkIfOpen(id){
	id = id.substring(1, id.length)
	var index
	var returnValue = true
	// Si no hay elementos metemos uno
	if(openElements.length == 0){
		openElements.push(id)
	}else{
		for(var i = 0; i < openElements.length; i++){
			// Si el id esta en el array lo eliminamos
			if(id == openElements[i]){
				index = openElements.indexOf(openElements[i]);
				openElements.splice(index, 1);				
				returnValue = false
				// document.getElementById('childOf' + id).style.display = 'none'
				break
			// Sino lo metemos
			}
		}
		if(returnValue){
			openElements.push(id)
		}
	}
	return returnValue
}


function goHome(){
	// Eliminamos
	clearContent()
	arrayAux = arrayData
	childsOfThisLevel = arrayData.length
	titles = []
	// // Creamos
	
	// // Posicionamos
	setTimeout(()=>{
		readOpml(json)
		positionOuterSpheres()
	}, 350)

}


function clearContent(){
	var circle = document.getElementsByClassName('circle')
	var sphereAdd = document.getElementsByClassName('sphereAdd')
	var center = document.getElementsByClassName('centerCircle')
	var cornerCircle = document.getElementsByClassName('cornerCircle')	
	var transitionTime = 350
	
	var i = 0
	if(circle.length != 0){
		if(cornerCircle.length != 0){
			cornerCircle[0].classList.add('goHome')
			setTimeout(()=>{
				cornerCircle[0].parentNode.removeChild(cornerCircle[0])
			}, transitionTime)
		}
		for(var i = 0; i < circle.length; i++){
			// console.log(i);
			circle[i].classList.add('goHome');
			sphereAdd[i].classList.add('goHome');
		}
		setTimeout(()=>{
			while(circle[0]) {
				// console.log('removing');
					circle[0].parentNode.removeChild(circle[0]);
					sphereAdd[0].parentNode.removeChild(sphereAdd[0]);
			}
			center[0].parentNode.removeChild(center[0])
		}, transitionTime)
	

	}


	
	
}

function getRandomColor() {
	recu += 30
	  var letters = '0123456789ABCDEF', color = '#';
	  for (var i = 0; i < 6; i++) {
	    color += letters[Math.floor(Math.random() * 16)];
	  }
	  return color;
}



