var radi = 250 + 1 // 1 = border
var radiusChild = 75
var spheresAdd = 0
var centerX = 0
var centerY = 0
var angle = 0
var posX = 0
var posY = 0
var home = true
var parent = "0"
var currentCenter = parent
var cornerTitle = ''

// Functions called just rigth load
// createAddButtons(5, '+')
// createSpheres(5, 'Title')
// createCenterSphere()
recalculatePosition()
positionHomeButton()


// BEGIN TEST ZONE
eventFire(document.getElementById('mindlyContent'), 'click');


function positionHomeButton(){
	document.getElementById('goHome').style.right = centerX * 2 * 0.05 + 'px'
	document.getElementById('goHome').style.bottom = centerY * 2 * 0.05 + 'px'
}

function eventFire(el, etype){
  if (el.fireEvent) {
    el.fireEvent('on' + etype);
  } else {
    var evObj = document.createEvent('Events');
    evObj.initEvent(etype, true, false);
    el.dispatchEvent(evObj);
  }
}

// END TEST ZONE
        
new ResizeSensor($('#mindlyContent'), function(){ 
    recalculatePosition()
});


// recalcula las posiciones cuando se hace un resize
function recalculatePosition(){
	// medidas para tomar el centro del documento
	centerX = parseInt($("#mindlyContent").width() / 2)
	centerY = parseInt($("#mindlyContent").height() / 2)

	// Posiciona el circulo de la esquina siempre alli si es que lo hay
	// positionCornerCircle(currentCenter)
	positionOuterSpheres()
}
function positionOuterSpheres(){
	spheresAdd = document.getElementsByClassName('sphereAdd')
	circles = document.getElementsByClassName('circle')
	var newArrayWithSpheresAndCircles = [];
	circles = document.getElementsByClassName('circle')
	var totalElements = spheresAdd.length * 2
	var degrees = 2 * Math.PI  / totalElements
	var newDegree = 0
	var newPosition;
	// odd numbers = circles , even numbers = spheresAdd, totalElements will be always odd
	for(var i = 0; i < totalElements / 2; i++){
		newArrayWithSpheresAndCircles.push(circles[i])
		newArrayWithSpheresAndCircles.push(spheresAdd[i])		
	}

	for(var i = 0; i < totalElements; i++){
		newPosition = recalculatePositionWithAngle(newDegree)
		newArrayWithSpheresAndCircles[i].classList.remove('hide')
		newArrayWithSpheresAndCircles[i].classList.add('show')
		if(i % 2 == 0 && i != 1){
			newArrayWithSpheresAndCircles[i].style.left = ( newPosition[0] - 75 ) + "px"
			newArrayWithSpheresAndCircles[i].style.top  = ( newPosition[1] - 75 ) + "px"
		}else{
			newArrayWithSpheresAndCircles[i].style.left = ( newPosition[0] - 25 ) + "px"
			newArrayWithSpheresAndCircles[i].style.top  = ( newPosition[1] - 25 ) + "px"
		}	
		newArrayWithSpheresAndCircles[i].classList.add('fade-in')
		newDegree += degrees
	}
}

function recalculatePositionWithAngle(angle){
	posX = parseInt(radi * Math.cos(angle) + centerX)
	posY = parseInt(radi * Math.sin(angle) + centerY)
	return [posX, posY]
}

// Mantiene el circulo de la esquina siempre en la esquina al resize
function positionCornerCircle(){
	// Reemplazar el 0 por el ID padre en cada caso
	if(!home){	
		console.log(parent.substring(0,parent.length - 1));


		document.getElementById(parent).classList.remove('circle')
		document.getElementById(parent).classList.remove('center')
		document.getElementById(parent).classList.remove('centerCircle')
		document.getElementById(parent).style.left = "0px"
		document.getElementById(parent).style.top = "0px"
		document.getElementById(parent).classList.add('move-ne')
		document.getElementById(parent).classList.add('cornerCircle')
		document.getElementById('title' + parent).classList.add('cornerTitle')
		document.getElementById('innerCenterCircleDiv' + parent).classList.add('innerCenterCircleDivFadeOut')
		removeParent()
	}
}


function removeParent(){
	if(parent.length > 1){
		var id = parent.substring(0, parent.length - 1)
		 $("#" + id).remove();
	}
}

// Reemplazar el 0 por el ID padre en cada caso
function moveToNextLevel(id, doRotation, doPositionCornerCircle){ 
	

	checkIfHome(id)
	if(doPositionCornerCircle){
		positionCornerCircle()
	}
	
	parent = id
	if(doRotation){
		rotate(id)
	 }else{

	 }
	createAddButtons(childsOfThisLevel, '+')
	createSpheres(childsOfThisLevel, 'Untitled')

	// document.getElementById(id).classList.remove('circle')

	// getInfo(parent)
}

function checkIfHome(id){
	if(id.length == 1){
		home = true
	}else{
		home = false
	}
}

function rotate(id){

		var thisCircle = document.getElementById(id)
		console.log(thisCircle);
		thisCircle.classList.remove('circle')
		$("#sphereAdd" + id).remove()
		easeInOutSpheres(id)	

		var leftOrRight = ""
		var newDegree = 0
		var newPosition = 0
		var degrees = 0
		var intervalRotate = setInterval(rote, 0);

		// currentDegree es la posicion en la que se encuentra contando todas las esferas
		// coincide con el ultimo valor del id
		var currentDegreeIndex = parseInt(id.charAt(id.length - 1)) + 1
		currentDegreeIndex == 0 ? currentDegreeIndex = 1 : null

		// el angulo del elemento es 360 / el numero de elementos * la posicion del elemento
		//  menos 360 / el numero de elementos. Sin esto nos devolveria la posicion del elemento 
		// con indice id + 1
		var	currentDegree = 360 / (spheresAdd.length + 1) * currentDegreeIndex - ( 360 / (spheresAdd.length + 1) )
		var currentCoords = aPartirDeLaPosicionEncontrarCoordenadas(currentDegreeIndex)

		/*A partir de las coordenadas, saber si va hacia la derecha o a la izquierda*/

		// Calculamos que coordenadas son las que corresponden a 225º, que es la posicion contraria a 45º
		var coordAt45Deg = checkCoordsAt45Degree()
		var coordAt225Deg = checkCoordsAt225Degree()
		
		/* Comprobamos si la esfera se movera hacia la derecha o hacia la izquierda*/
		var leftOrRight = checkIfRightOrLeft(currentCoords[0], currentCoords[1], coordAt45Deg, coordAt225Deg)

		var degrees = (2 * Math.PI  / (spheresAdd.length + 1))
		var gradoEsfera = (degrees * id.substring(id.length - 1, id.length))
		var totalDegree = (2 * Math.PI / 360)
		var newDegree = gradoEsfera
		var newCoords = 0
		var leftLimit = newDegree * 180
		var i = 0
		function rote(){
			newCoords = recalculatePositionWithAngle(newDegree)
			if(leftOrRight == 'right'){
				if(parseFloat(newDegree.toFixed(8)) >= parseFloat(gradoEsfera.toFixed(8)) ){
					thisCircle.style.top = (newCoords[1] - 75) + "px"
					thisCircle.style.left = (newCoords[0] - 75) + "px"
				}
				newDegree += totalDegree
			}else{
				if(parseFloat(newDegree.toFixed(8)) >= parseFloat(gradoEsfera.toFixed(8)) ){
					thisCircle.style.top = (newCoords[1] - 75) + "px"
					thisCircle.style.left = (newCoords[0] - 75) + "px"
				}
				newDegree += (leftLimit - totalDegree)
			}
			if((newCoords[0] - 75 - 0 <= coordAt45Deg[0] && newCoords[0] - 75 + 0 >= coordAt45Deg[0]) && 
			(newCoords[1] - 75 - 0 <= coordAt45Deg[1] && newCoords[1] - 75 + 0 >= coordAt45Deg[1] ) ){
				thisCircle.classList.add('center', 'centerCircle')
			thisCircle.classList.remove('circle')
				clearInterval(intervalRotate)
			}
		}	
	
}



function removePreviousSpheres(id, newArrayWithSpheresAndCircles){
		for(var i = 0; i < length; i++){
			if(newArrayWithSpheresAndCircles[i].id != id){
				$("#"+ newArrayWithSpheresAndCircles[i].id).fadeOut(300, function(){ 
				    $(this).remove();
				});
			}else{
			}
		}
}

function easeInOutSpheres(id){
	var spheresAdd = document.getElementsByClassName('sphereAdd')
	var length = (spheresAdd.length * 2) - 1
	var circles = document.getElementsByClassName('circle')
	// var currentDegreeIndex = parseInt(id.charAt(id.length - 1))
	var newArrayWithSpheresAndCircles = [];

	for(var i = 0; i < length; i++){
			newArrayWithSpheresAndCircles.push(circles[i])
			newArrayWithSpheresAndCircles.push(spheresAdd[i])	
	}

console.log(newArrayWithSpheresAndCircles);

	// obtener id de las esferas y compararlos
	for(var i = 0; i < spheresAdd.length * 2; i++){
		// if(newArrayWithSpheresAndCircles[i].id != id){
			$("#"+ newArrayWithSpheresAndCircles[i].id).fadeOut(300, function(){ 
				    $(this).remove();
				});
		// }
	}
}
 

function aPartirDeLaPosicionEncontrarCoordenadas(id){
	var totalEspheres = spheresAdd.length * 2
	var selectedByAbsolutePosition = id * 2 - 1
	var degrees = 2 * Math.PI  / totalEspheres
	var degree = 0
	var degree = degrees * ( selectedByAbsolutePosition - 1)
	// Coordenadas de la esfera actual
	var currentCoordByAbsolutePosition = recalculatePositionWithAngle(degree)
	var coordX = currentCoordByAbsolutePosition[0] - 75
	var coordY = currentCoordByAbsolutePosition[1] - 75
	return [coordX, coordY]
}

function checkIfRightOrLeft(coordX, coordY, coordAt45Deg, coordAt225Deg){
	// si esta en el 5o octavo de la esfera empezando desde el 0
	if(coordX < coordAt225Deg[0] && ((coordY + 75) <= centerY)){
		return 'left'
	// 3r y 4o octavo
	}else if(coordX <= centerX - 75){
		return 'left'
	// 2o octavo
	}else if((coordX <= coordAt45Deg[0]) && ((coordY + 75) >= centerY + (centerY - coordAt45Deg[1]))){
		return 'left'
	// Esta a la derecha
	}else{

		return 'right'
	}
}

function getLastCornerTitle(id){
	cornerTitle = document.getElementById(id).innerText
	console.log('-------------------------' + cornerTitle);
}

function checkCoordsAt45Degree(){
	/* Returns the coordinates at 45 degrees*/
	// With 8 elements, the first add button is at 45 degrees
	var degrees = 2 * Math.PI  / 8
	var newDegree = 0
	var newPosition;
	for(var i = 0; i < 2; i++){
		newPosition = recalculatePositionWithAngle(newDegree)
		newPosition[0] = newPosition[0] - 75
		newPosition[1] = newPosition[1] - 75
		newDegree += degrees
	}
	return newPosition
}

function checkCoordsAt225Degree(){
	/* Returns the coordinates at 45 degrees*/
	// With 8 elements, the first add button is at 45 degrees
	var degrees = 2 * Math.PI  / 8
	var newDegree = 0
	var newPosition;
	for(var i = 0; i < 6; i++){
		newPosition = recalculatePositionWithAngle(newDegree)
		newPosition[0] = newPosition[0] - 75
		newPosition[1] = newPosition[1] - 75
		newDegree += degrees
	}
	return newPosition
}


function toggleStick(){
	var c = null
	var ctx = null
	c = document.getElementById("canvas");
	c.width = centerX
	c.height = centerY
	ctx = c.getContext('2d')
	ctx.beginPath()
	ctx.strokeStyle = "gainsboro"
	ctx.moveTo(0,0)

	ctx.moveTo(0, 0);
	if(!home){
		c.width = centerX
		c.height = centerY
		ctx.lineTo(0,0)
		ctx.stroke()
		ctx.closePath()
	}else{
		ctx.lineTo((centerX/2),(centerY/2))
		ctx.stroke()
		ctx.closePath()
	}
}

// function savePreviousCornerTitle(id){
	
// 	var parent = id.substring(0, id.length - 1)
// 	console.log(id, 'title' + parent);
// 	cornerTitle = document.getElementById('title' + id).innerText
// 	console.log(cornerTitle);
// }

function createAddButtons(qty, text = '+'){
	for(var i = 0; i < qty; i++){
		var button = document.createElement('button')
		button.setAttribute('id', 'sphereAdd'+ currentCenter + '' + i)
		button.classList.add('sphereAdd', 'fadeOut')
		var title = document.createTextNode(text)
		button.appendChild(title)
		document.getElementById('mindlyContent').appendChild(button)
	}
}

function createCornerSphere(parent){
	var button = document.createElement('button')
	button.setAttribute('id', parent)
	button.classList.add('cornerCircle', 'move-ne')
	var innerCenterCircleDiv = document.createElement('div')
	innerCenterCircleDiv.setAttribute('id', 'innerCenterCircleDiv' +  parent)
	innerCenterCircleDiv.classList.add('innerCenterCircleDivFadeOut', 'innerCenterCircleDiv')
	var title = document.createElement('span')
	var titleText = document.createTextNode(cornerTitle)
	title.setAttribute('id', 'title' + parent)
	title.classList.add('cornerTitle')
	title.appendChild(titleText)
	innerCenterCircleDiv.appendChild(title)
	button.appendChild(innerCenterCircleDiv)
	button.style.left = 0 + "px"
	button.style.top = 0 + "px"	
	document.getElementById('mindlyContent').appendChild(button)
}

function createSpheres(qty, text = 'Title'){
	for(var i = 0; i < qty; i++){
		console.log(currentCenter);
		var button = document.createElement('button')
		button.setAttribute('id', currentCenter + ""+ i)
		button.classList.add('circle', 'fadeOut', 'element')
		var innerCenterCircleDiv = document.createElement('div')
		innerCenterCircleDiv.setAttribute('id', 'innerCenterCircleDiv' +  currentCenter + ""+ i)
		innerCenterCircleDiv.classList.add('innerCenterCircleDiv')
		var title = document.createElement('span')
		var titleText = document.createTextNode(text + " " + currentCenter + "" + i)
		title.setAttribute('id', 'title' + currentCenter + ""+ i)
		title.appendChild(titleText)
		innerCenterCircleDiv.appendChild(title)
		button.appendChild(innerCenterCircleDiv)
		document.getElementById('mindlyContent').appendChild(button)
	}
}

function createCenterSphere(text = 'Untitled', id){
		var button = document.createElement('button')
		button.setAttribute('id', id)
		button.classList.add('centerCircle', 'prev', 'center', 'element', 'fadeOut', 'fade-in')
		var innerCenterCircleDiv = document.createElement('div')
		innerCenterCircleDiv.setAttribute('id', 'innerCenterCircleDiv' + id)
		innerCenterCircleDiv.classList.add('innerCenterCircleDiv')
		var title = document.createElement('span')
		var titleText = document.createTextNode(text)
		title.setAttribute('id', 'title' + id)
		title.appendChild(titleText)
		innerCenterCircleDiv.appendChild(title)
		button.appendChild(innerCenterCircleDiv)
		document.getElementById('mindlyContent').appendChild(button)
}

document.body.addEventListener( 'click', function ( event ) {


console.log(titles)

	var x = event.srcElement.className
	var id = event.srcElement.id
	var prev = document.getElementsByClassName('prev')
	currentCenter = id
	console.log(id);
	var s = x.split(" ")
	// savePreviousCornerTitle(id)
	for(var i = 0; i < s.length; i++){
		if(s[i] == 'circle'){

			if(prev.length > 0){
				prev[0].parentNode.removeChild(prev[0])
			}
		   	test = 1
				 var hasChilds = setNewLevel(id)
				 
			if(hasChilds){
				moveToNextLevel(id, true, true)
				manageTitles('add')
				getInfo(id)
				setTimeout(()=>{
					positionOuterSpheres()
				},500)
			}

			
		}
		else if(s[i] == 'cornerCircle'){
			manageTitles('substract')
			if(id.length == 1){
				goHome()
			}else{				
				// console.log(prev);
				// if(prev.length > 0){
				// 	console.log('eliminar')
				// 	prev[0].parentNode.removeChild(prev[0])
				// }
				if(id.length >= 3){
					console.log('get corner');
					// alert(titles[id.length - 2] + ' 1')
					cornerTitle =  titles[id.length - 2]
				}else{
					cornerTitle =  titles[0]
					// alert(titles[0] + ' 2')
				}

				
				// else if(id.length == 2){
				// 	console.log('going home');
				// 	goHome()
				// 	break
				// }
				var parentId = id.substring(0, id.length - 1)
				var parentTitle = $("#title" + id).text()
				clearContent()

				setTimeout(()=>{
					
					createCornerSphere(parentId)
					// console.log('Estableciendo nuevo nivel');
					setNewLevel(id)
					// console.log('Moviendo al siguiente nivel');
					
					moveToNextLevel(parentId, false, false)
					// console.log('Obteniendo la informacion de ' + id);
					getInfo(id)
					// console.log('Creando efera central');
					createCenterSphere(parentTitle, id)
					// console.log('Posicionando esferas');
					positionOuterSpheres()
				},500)
			}
		}
	}
} );

