<!DOCTYPE html>
<html lang="en" style="width: 100%; height: 100%">
	<head>
		<meta charset="UTF-8">
		<title>Document</title>
		<link rel="stylesheet" href="style/style.css">
		<script	src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	</head>
	<body style="width: 100%; height: 100%; margin: 0px !important" >
					
		<div id="mindlyContent">
			<button id="goHome" onclick="goHome()">
				<i class="material-icons">home</i>
			</button>
			<canvas id="canvas"></canvas>
			<div id="cicleShape"></div>
		<!-- <?php
			/*require('./server/upload_file.php');*/
		?>  -->
		<!-- <input type="text" hidden id="myFileId" value=<?php echo $id; ?>> -->
		<!-- <input type="text" id="myFileId" value="<?php echo $id ?>" /> -->

		</div>
		
		<script src="script/ResizeSensor.js"></script>
		<script src="script/mindly.js"></script>
		<script src="script/script.js"></script>
	</body>
</html>